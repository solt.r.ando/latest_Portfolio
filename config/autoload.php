<?php

spl_autoload_register(function($class) {

  $prefix = 'MyApp\\';

  // もし MyApp　から始まっていれば
  if (strpos($class, $prefix) === 0) {

    // 6文字目から数えてクラス名を引っ張ってくる
    $className = substr($class, strlen($prefix));
    // クラスのディレクトリの作成
    $classFilePath = (dirname(__DIR__) . '/../lib/' . str_replace('\\', '/', $className) .'.php');

    if (file_exists($classFilePath)) {

      require $classFilePath;

    } else {

      echo 'No such class: ' . $className . "<br>";
      echo __DIR__ . "<br>";
      echo ($classFilePath) . "<br>";
      print_r($class);

    }

  }

});
