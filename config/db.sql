create databese portfolio;

grant all on portfolio .* to andrew@localhost IDENTIFIED BY 'andrew135';



  create table users (
    id int not null auto_increment primary key,
    name varchar(20),
    email varchar(50) unique key,
    password varchar(100),
    tel varchar(20),
    prefectures varchar(50),
    hobby varchar(200),
    gender enum('男性', '女性'),
    contact varchar(255),
    creation_time  DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
  );


create table country(
  id int(11) NOT NULL auto_increment PRIMARY KEY,
    rank varchar(255) NOT NULL,
    nation varchar(255) NOT NULL,
    food varchar(255) NULL,
    reason varchar(255) NULL
    );


    Alter table users  MODIFY COLUMN gender varchar(10);
