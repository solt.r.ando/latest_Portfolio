<?php

namespace lib;

class MyValidation  {

  private $error = [];


/*
  アカウント メソッドの定義
*/
public function check_Account_Form($key, $inputValue) {

  $this->checkName($key, $inputValue);
  $this->checkEmail($key, $inputValue);
  $this->checkPassword($key, $inputValue);
  $this->checkTel($key, $inputValue);
  $this->checkPrefectures($key, $inputValue);

  return $this->error;

  }


/*
  アカウント内容(name,emailなど、それぞれに必要なもの)
  のメソッドをまとめたもの
*/

  // 名前(name)のバリデーション
  public function checkName($key, $inputValue) {

     $this->checkRequired($key, $inputValue);

     return $this->error;

    }


  // メールアドレスのバリデーション
  public function checkEmail($key, $inputValue) {

    $this->checkRequired($key, $inputValue);
    $this->checkEmailFormat2($key, $inputValue);

    return $this->error;

    }


    // パスワードのバリデーション
    public function checkPassword($key, $inputValue) {

      $this->checkRequired($key, $inputValue);
      $this->checkLength($key, $inputValue);
      $this->checkHalfsize($key, $inputValue);

      return $this->error;

      }


    // 電話番号のバリデーション
    public function checkTel($key, $inputValue) {

      $this->checkRequired($key, $inputValue);
      $this->checkFormat($key, $inputValue);
      $this->checkTelLength($key, $inputValue);
      $this->checkTelHalfsize($key, $inputValue);

      return $this->error;

    }

    // 都道府県のバリデーション
    public function checkPrefectures($key, $inputValue) {

      $this->checkPrefecturesRequired($key, $inputValue);

      return $this->error;

    }



    /*
        行ってみたい国の、メソッドの定義
    */

    public function Nation($key, $inputValue) {

      $this->checkNation($key, $inputValue);
      $this->checkReason($key, $inputValue);

      return $this->error;

      }


      /*
          行ってみたい国の内容
          国名、理由のメソッドをまとめたもの
      */

      // 国名のバリデーション
      public function checkNation($key, $inputValue) {

        $this->checkRequired($key, $inputValue);

        return $this->error;

      }

      // 理由のバリデーション
      public function checkReason($key, $inputValue) {

        $this->checkRequired($key, $inputValue);

        return $this->error;

      }





    /*
      一つ一つのメソッド内容
    */

    // 必須項目の確認
    public function checkRequired($key, $inputValue) {

      if (empty($inputValue)) {

        $this->error[] = $key.'は必須項目です。';

      }

    }


    // メールアドレスの形式チェック
    public function checkEmailFormat($key, $inputValue) {

      if (!preg_match('/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/', $inputValue) && $inputValue !== '') {

        return $this->error[] = $key.'の形式に間違いがあります。';

      }

    }


    // メールアドレスの形式チェック2
    public function checkEmailFormat2($key, $inputValue) {

      if (!filter_var($inputValue, FILTER_VALIDATE_EMAIL) && $inputValue != '') {

        return $this->error[] = $key.'の形式に間違いがあります。';

      }

    }


    // 文字の長さ確認
    public function checkLength($key, $inputValue) {


      if (preg_match('/^[a-zA-Z1-9]{1,7}$/', $inputValue) ) {

        return $this->error[] = $key.'は8文字以上で入力してください。';

      }

    }


    // 半角チェック
    public function checkHalfsize($key, $inputValue) {

      if (!preg_match('/^[a-zA-Z0-9]+$/', $inputValue) && $inputValue !== '') {

        return $this->error[] = $key.'は半角で入力してください。';

      }

    }

    //　電話番号　半角チェック
    public function checkTelHalfsize($key, $inputValue) {

      if (!preg_match('/^[0-9]+$/', $inputValue) && $inputValue !== '') {

        return $this->error[] = $key.'は半角数字で入力してください。';

      }

    }


     // 形式(ハイフン)チェック
     public function checkFormat($key, $inputValue) {

       if (preg_match('/[-]+/', $inputValue)) {

         return $this->error[] = $key.'はハイフンなしで入力してください。';

       }

     }


     // 文字数チェック(10文字 or 11文字)
     public function checkTelLength($key, $inputValue) {

     if (preg_match('/^[0-9]{1,9}+$/', $inputValue)) {


       return $this->error[] = $key.'は10文字か11文字で入力してください。';

     } elseif (strlen($inputValue) >= 12 && preg_match('/^[0-9]+$/', $inputValue)) {

       return $this->error[] = $key.'は10文字か11文字で入力してください。';

     }

   }


// 都道府県のバリデーション
public function checkPrefecturesRequired($key, $inputValue) {

  // 必須項目の確認
    if ($inputValue === '選択') {

      $this->error[] = $key.'は必須項目です。';

    }


}






}
