<?php

namespace lib\Model;

class User Extends \lib\Model {

  // ユーザー作成のメソッド
  public function create() {

    //　プリペアドステートメント
    $stmt = $this->dbh->prepare("INSERT INTO users (name, email, password, tel, prefectures, hobby, gender, contact, creation_time)
    VALUES (:name, :email, :password, :tel, :prefectures, :hobby, :gender, :contact, now())");

      if ($stmt) {

        // プレースホルダへ実際の値を設定する
        if (isset($_SESSION['name']) || isset($_SESSION['email']) || isset($_SESSION['password'])
            || isset($_SESSION['tel']) || isset($_SESSION['prefectures']) || isset($_SESSION['prefectures'])
            || isset($_SESSION['hobby'])|| isset($_SESSION['gender']) || isset($_SESSION['contact']))  {


        $stmt->bindValue(':name', $_SESSION["name"], \PDO::PARAM_STR);
        $stmt->bindValue(':email', $_SESSION["email"], \PDO::PARAM_STR);
        $stmt->bindValue(':password', password_hash($_SESSION['password'], PASSWORD_DEFAULT), \PDO::PARAM_STR);
        $stmt->bindValue(':tel', $_SESSION["tel"], \PDO::PARAM_STR);
        $stmt->bindValue(':prefectures', $_SESSION["prefectures"], \PDO::PARAM_STR);
        $stmt->bindValue(':hobby', $_SESSION["hobby"], \PDO::PARAM_STR);
        $stmt->bindValue(':gender', $_SESSION["gender"], \PDO::PARAM_STR);
        $stmt->bindValue(':contact', $_SESSION["contact"], \PDO::PARAM_STR);

          }

      }

      // 実行する
      $stmt->execute();



  }




  // ログインのメソッド
  public function UserLogin() {

    $email = $_SESSION['email'];
    $password = $_SESSION['password'];

    // アカウントで検索
    $statement = $this->dbh->prepare("SELECT * FROM users WHERE email = (:email)");
    $statement->bindValue(":email", $email, \PDO::PARAM_STR);
    $statement->execute();


    // アカウントが一致
    if ($row = $statement->fetch()) {

        $password_hash = $row['password'];
        $userId = $row['userId'];
        $tel = $row['tel'];
        $name = $row['name'];
        $admin_role = $row['admin_role'];

        // パスワードが一致
        if (password_verify($password, $password_hash)) {

            // セッションハイジャック対策
            session_regenerate_id(true);

            $_SESSION['userId'] = $userId;
            $_SESSION['admin_role'] = $admin_role;

            // パスワードをセッションから解除
            unset($_SESSION['password']);
            unset($_SESSION['email']);

            header("Location: login_users.php");
            exit();

        } else {

            $_SESSION['not_match'] = "メールアドレスまたはパスワードが一致しません。";


        }

      } else {

          $_SESSION['not_match'] = "メールアドレスまたはパスワードが一致しません。";


        }

      }







  }
