<?php

namespace lib;

require_once('../config/db.php');

class Model {

  protected $dbh;

  public function __construct() {

    try {

      $this->dbh = new \PDO(DSN, DB_USERNAME, DB_PASSWORD);

      //例外処理を投げる（スロー）ようにする
      $this->dbh->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

    } catch(PDOException $e) {

      echo $e->getMessage();
      exit;

    }

  }


}
