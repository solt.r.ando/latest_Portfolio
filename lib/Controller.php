<?php

namespace lib;

class Controller {

  private $_errors;
  private $_values;


  // コンストラクタ
  public function __construct() {

    if (!isset($_SESSION['token'])) {

      $_SESSION['token'] = bin2hex(openssl_random_pseudo_bytes(16));

    }

    $this->_errors = new \stdClass();
    $this->_values = new \stdClass();


  }



  //　入力した値のセット
  protected function setValues($key, $value) {

    $this->_values->$key = $values;

  }

  //　入力値したの取得
  public function getValues() {

    return $this->_values;

  }

  // エラーのセット
  protected function setErrors($key, $error) {

    $this->_errors->$key = $error;

  }

  // エラーの取得
  public function getErrors($key) {

    return isset($this->_errors->$key) ? $this->_errors->$key : '';

  }

  // エラーを持ってるか
  protected function hasError() {

    return !empty(get_object_vars($this->_errors));

  }




  // バリデーションメソッド
  protected function _validate() {

    // トークンが一致しているか
  if (!isset($_POST['token']) || $_POST['token'] !== $_SESSION['token']) {

    echo "Invalid Token!";
    exit;

  }
}


  /*
    アカウント内容(name,emailなど、それぞれに必要なもの)
    のメソッドをまとめたもの
  */

  public function checkAccount($key, $inputValue) {

  $this->checkName($key, $inputValue);
  $this->checkEmail($key, $inputValue);
  $this->checkPassword($key, $inputValue);
  $this->checkTel($key, $inputValue);
  $this->checkPrefectures($key, $inputValue);

  return $this->error;

  }

    // 名前(name)のバリデーション
    protected function checkName($key, $inputValue) {

      $this->checkRequired($key, $inputValue);

       return $this->error;

    }


    // メールアドレスのバリデーション
    protected function checkEmail($key, $inputValue) {

      $this->checkRequired($key, $inputValue);
      $this->checkEmailFormat2($key, $inputValue);

       return $this->error;

      }


      // パスワードのバリデーション
    protected function checkPassword($key, $inputValue) {

        $this->checkRequired($key, $inputValue);
        $this->checkLength($key, $inputValue);
        $this->checkHalfsize($key, $inputValue);

         return $this->error;

        }


      // 電話番号のバリデーション
      protected function checkTel($key, $inputValue) {

        $this->checkRequired($key, $inputValue);
        $this->checkFormat($key, $inputValue);
        $this->checkTelLength($key, $inputValue);
        $this->checkTelHalfsize($key, $inputValue);

        return $this->error;

      }

      // 都道府県のバリデーション
      protected function checkPrefectures($key, $inputValue) {

        $this->checkPrefecturesRequired($key, $inputValue);

        return $this->error;

      }



      /*
          行ってみたい国の、メソッドの定義
      */

      protected function Nation($key, $inputValue) {

        $this->checkNation($key, $inputValue);
        $this->checkReason($key, $inputValue);

        return $this->error;

        }


        /*
            行ってみたい国の内容
            国名、理由のメソッドをまとめたもの
        */

        // 国名のバリデーション
        protected function checkNation($key, $inputValue) {

          $this->checkRequired($key, $inputValue);

          return $this->error;

        }

        // 理由のバリデーション
        protected function checkReason($key, $inputValue) {

          $this->checkRequired($key, $inputValue);

          return $this->error;

        }





      /*
        一つ一つのメソッド内容
      */

      // 必須項目の確認
      protected function checkRequired($key, $inputValue) {

        if (empty($inputValue)) {

          $this->error = [];
          $this->error[] = $key.'は必須項目です。';

        } else {

          $this->error = [];

        }

      }


      // メールアドレスの形式チェック
      protected function checkEmailFormat($key, $inputValue) {

        if (!preg_match('/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/', $inputValue) && $inputValue !== '') {

          $this->error = [];
          return $this->error[] = $key.'の形式に間違いがあります。';

        }

      }


      // メールアドレスの形式チェック2
      protected function checkEmailFormat2($key, $inputValue) {

        if (!filter_var($inputValue, FILTER_VALIDATE_EMAIL) && $inputValue != '') {

          $this->error = [];
          return $this->error[] = $key.'の形式に間違いがあります。';

        }

      }


      // 文字の長さ確認
      protected function checkLength($key, $inputValue) {


        if (preg_match('/^[a-zA-Z1-9]{1,7}$/', $inputValue) ) {

          $this->error = [];
          return $this->error[] = $key.'は8文字以上で入力してください。';

        }

      }


      // 半角チェック
      protected function checkHalfsize($key, $inputValue) {

        if (!preg_match('/^[a-zA-Z0-9]+$/', $inputValue) && $inputValue !== '') {

          $this->error = [];
          return $this->error[] = $key.'は半角で入力してください。';

        }

      }

      //　電話番号　半角チェック
      protected function checkTelHalfsize($key, $inputValue) {

        if (!preg_match('/^[0-9]+$/', $inputValue) && $inputValue !== '') {

          $this->error = [];
          return $this->error[] = $key.'は半角数字のみで入力してください。';

        }

      }


       // 形式(ハイフン)チェック
       protected function checkFormat($key, $inputValue) {

         if (preg_match('/[-]+/', $inputValue)) {

           $this->error = [];
           return $this->error[] = $key.'はハイフンなしで入力してください。';

         }

       }


       // 文字数チェック(10文字 or 11文字)
       protected function checkTelLength($key, $inputValue) {

       if (preg_match('/^[0-9]{1,9}+$/', $inputValue)) {

         $this->error = [];
         return $this->error[] = $key.'は10文字か11文字で入力してください。';

       } elseif (strlen($inputValue) >= 12 && preg_match('/^[0-9]+$/', $inputValue)) {

         $this->error = [];
         return $this->error[] = $key.'は10文字か11文字で入力してください。';

       }

     }


  // 都道府県のバリデーション
  protected function checkPrefecturesRequired($key, $inputValue) {

    // 必須項目の確認
      if ($inputValue === '選択') {

        $this->error = [];
        return $this->error[] = $key.'は必須項目です。';

      } else {

        $this->error = [];

      }


  }









}
