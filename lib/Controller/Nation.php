<?php

namespace lib\Controller;


class Nation_register extends \lib\Controller {


  public function run() {


    if ($_SERVER['REQUEST_METHOD'] === 'POST') {

      $this->postProcess();

    }
  }


  protected function postProcess() {

    // バリデーションエラーを変数に代入
    // 必須項目のバリデーション
    $nation_errors = $Nation->checkNation('国名', $nation);
    $reason_errors = $Nation->checkReason('理由', $reason);

    // 値をセッションに保持
    $_SESSION['nation'] = filter_input(INPUT_POST, 'nation');
    $_SESSION['reason'] = filter_input(INPUT_POST, 'reason');


    // もしエラーがあれば、登録フォームにリダイレクト
    if (count($name_errors) || count($email_errors) > 0) {

      $_SESSION['nation_errors']  = $nation_errors;
      $_SESSION['reason_errors']  = $reason_errors;

      header('Location: ' . SITE_URL . '/nation_registration_form.php');


    }


  }

  // DBに登録
  public function db_Connect() {

    //配列を文字列に変換する(DBに登録した配列を使う時は、逆にデコードをする)
    $hobbyData = $_SESSION["hobby"];
    $_SESSION["hobby"] = serialize($hobbyData);

      try {

        $userModel = new \lib\Model\User();
        $userModel->create();

      } catch(PDOException $e) {

        $this->setErrors('email', $e->getMessage());
        return;

      }


  }

}
