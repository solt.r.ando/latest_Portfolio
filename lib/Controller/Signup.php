<?php

namespace lib\Controller;


class Signup extends \lib\Controller {


  public function run() {


    if ($_SERVER['REQUEST_METHOD'] === 'POST') {

      $this->postProcess();

    }
  }


  protected function postProcess() {

    // バリデーションエラーを変数に代入
    $name_errors = $this->checkName('名前', $_POST['name']);
    $email_errors = $this->checkEmail('メールアドレス', $_POST['email']);
    $password_errors = $this->checkPassword('パスワード', $_POST['password']);
    $tel_errors = $this->checkTel('電話番号', $_POST['tel']);
    $prefectures_errors = $this->checkPrefectures('都道府県', $_POST['prefectures']);

    // 値をセッションに保持
    $_SESSION['name'] = filter_input(INPUT_POST, 'name');
    $_SESSION['email'] = filter_input(INPUT_POST, 'email');
    $_SESSION['password'] = filter_input(INPUT_POST, 'password');
    $_SESSION['tel'] = filter_input(INPUT_POST, 'tel');
    $_SESSION['prefectures'] = filter_input(INPUT_POST, 'prefectures');
    $_SESSION['hobby'] = filter_input(INPUT_POST, 'hobby', FILTER_DEFAULT,FILTER_REQUIRE_ARRAY);
    $_SESSION['gender'] = filter_input(INPUT_POST, 'gender');
    $_SESSION['contact'] = filter_input(INPUT_POST, 'contact');


    // もしエラーがあれば、登録フォームにリダイレクト
    if (count($name_errors) || count($email_errors) || count($password_errors)
    || count($tel_errors) || count($prefectures_errors) > 0) {

      $_SESSION['name_errors']  = $name_errors;
      $_SESSION['email_errors']  = $email_errors;
      $_SESSION['password_errors']  = $password_errors;
      $_SESSION['tel_errors']  = $tel_errors;
      $_SESSION['prefectures_errors']  = $prefectures_errors;

      header('Location: ' . SITE_URL . '/registration_form.php');


    }



  }

  // DBに登録
  public function db_Connect() {

    //配列を文字列に変換する(DBに登録した配列を使う時は、逆にデコードをする)
    $hobbyData = $_SESSION["hobby"];
    $_SESSION["hobby"] = serialize($hobbyData);

      try {

        $userModel = new \lib\Model\User();
        $userModel->create();

      } catch(PDOException $e) {

        $this->setErrors('email', $e->getMessage());
        return;

      }


  }

}
