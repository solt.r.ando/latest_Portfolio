<?php

 namespace lib\Controller;

class Login extends \lib\Controller {


  public function Login() {

    // ログインボタンが押された場合
    if (isset($_POST["login"])) {

      $this->postProcess();

      }
    }


    protected function postProcess() {

      // POSTされたデータを各変数に入れる
       $email = filter_input(INPUT_POST, 'email');
       $password = filter_input(INPUT_POST, 'password');

       $_SESSION['email'] = $email;
       $_SESSION['password'] = $password;

       // 前後にある半角全角スペースを削除
       $email = Trim($email);
       $password = Trim($password);

       // ログイン時のバリデーション
      $email_errors = $this->emailValidate();
      $password_errors = $this->passwordValidate();


       // もしエラーがあれば
       if (count($email_errors) || count($password_errors) > 0) {

         $_SESSION['email_errors']  = $email_errors;
         $_SESSION['password_errors']  = $password_errors;
         header('Location:' . SITE_URL . '/login_form.php');
         exit;

       } else {

         unset($_SESSION['email_errors']);
         unset($_SESSION['password_errors']);

         // エラーがなければ実行する
           try {

             $userModel = new \lib\Model\User();
             $userModel->UserLogin();

             // エラーがある場合
             if (count($_SESSION['not_match']) > 0) {

               header('Location:' . SITE_URL . '/login_form.php');

             } else {

               unset($_SESSION['not_match']);

             }


           } catch(PDOException $e) {

             echo $e->getMessage();
             exit;

           }

       }







    }


    // ログイン時のメールアドレスとパスワードのバリデーション
    private function emailValidate() {

    // アカウント入力判定
    if (empty($_POST['email'])) {

         return $this->errors["email"] = "メールアドレスが入力されていません。";

      } elseif (!empty($_POST['email']) && !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {

        return $this->errors["email"] = "メールアドレスを正しく入力してください。";

    }

  }


  private function passwordValidate() {



      // パスワード入力判定
      if (empty($_POST['password'])) {

        return $this->errors["password"] = "パスワードが入力されていません。";

      } elseif (preg_match('/^[0-9a-zA-Z]{1,7}$/', $_POST['password'])) {

        return $this->errors["password"] = "パスワードは8文字以上で入力してください。";

      } else {

        $password_hide = str_repeat('*', strlen($_POST['password']));

      }

  }



}
