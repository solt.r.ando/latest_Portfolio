<?php
// 開発時にこれを書かないのはNG
ini_set('display_errors', TRUE);
error_reporting(E_ALL);

session_start();

header("Content-type: text/html; charset=utf-8");

require_once("../config/db.php");

$errors = '';

//配列を文字列に変換する(DBに登録した配列を使う時は、逆にデコードをする)
$hobbyData = $_SESSION["hobby"];
$_SESSION["hobby"] = serialize($hobbyData);

// タイム・ゾーンの設定
date_default_timezone_set('Asia/Tokyo');
$date = new DateTime();
$date = $date->format('Y-m-d H:i:s');

$password = $_SESSION["password"];


try {

  //例外処理を投げる（スロー）ようにする
  $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  $statement = $dbh->prepare("INSERT INTO users (name, email, password, tel, prefectures, hobby, gender, contact, creation_time)
  VALUES (:name, :email, :password, :tel, :prefectures, :hobby, :gender, :contact, :creation_time)");

  $password = password_hash($password, PASSWORD_DEFAULT);

  // トランザクション開始
  $dbh->beginTransaction();

  if ($statement) {

    // プレースホルダへ実際の値を設定する
    $statement->bindValue(':name', $_SESSION["name"], PDO::PARAM_STR);
    $statement->bindValue(':email', $_SESSION["email"], PDO::PARAM_STR);
    $statement->bindValue(':password', $password, PDO::PARAM_STR);
    $statement->bindValue(':tel', $_SESSION["tel"], PDO::PARAM_STR);
    $statement->bindValue(':prefectures', $_SESSION["prefectures"], PDO::PARAM_STR);
    $statement->bindValue(':hobby', $_SESSION["hobby"], PDO::PARAM_STR);
    $statement->bindValue(':gender', $_SESSION["gender"], PDO::PARAM_STR);
    $statement->bindValue(':contact', $_SESSION["contact"], PDO::PARAM_STR);
    $statement->bindValue(':creation_time', $date, PDO::PARAM_STR);


    //　実行する
    if (!$statement->execute()) {
      $errors['error'] = "登録失敗しました。 ";
    }

    // トランザクションコミット
    $dbh->commit();



  }


} catch (PDOException $e) {

    $dbh->rollBack();
    print('Error:'.$e->getMessage());
		$errors['error'] = "データベース接続失敗しました。";
    
}


// セッション変数を全て解除
$_SESSION = array();

// セッションを破棄する
session_destroy();




//
// Twig
//


// Composerで作成されたautoload.phpを読み込む
require_once('../vendor/autoload.php');
// Twig_loader_Filesystemを使う。registration_insert.phpからtemplatesディレクトリを指定。(相対パス)
$loader = new Twig_Loader_filesystem('../templates');
// $loaderをTwigの環境設定として Twig instance を生成
$twig = new Twig_Environment($loader);

// render
echo $twig->render('registration_insert.html', array(

  'hobbyData' => $hobbyData,
  'dbh' => $dbh,
  'statement' => $statement,
  'date' => $date,
  'errors' => $errors,
  'password' => $password

  )
);
