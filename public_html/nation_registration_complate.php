<?php

require_once('./functions.php');
require_once("../config/db.php");
require_once('../config/config.php');


// POST時
if (filter_input(INPUT_SERVER, "REQUEST_METHOD") == "POST") {


  $nation = $_SESSION["nation"];
  $reason = $_SESSION["reason"];
  $userId = $_SESSION['userId'];




    try {

      $dbh = new PDO($dsn, $user, $password);

      // プリペアドステートメント
      $statement = $dbh->prepare("INSERT INTO country (nation, reason, userId)
      VALUES (:nation, :reason, :userId)");

      // トランザクション開始
      $dbh->beginTransaction();


        if ($statement) {

          // プレースホルダへ実際の値を設定する
          $statement->bindValue(":nation", $nation, PDO::PARAM_STR);
          $statement->bindValue(":reason", $reason, PDO::PARAM_STR);
          $statement->bindValue(":userId", $userId, PDO::PARAM_STR);

          if (!$statement->execute()) {

            $errors["error"] = "データベース接続失敗しました。";

        }

          // トランザクションコミット
          $dbh->commit();

    }

} catch(PDOException $e) {

    $dbh->rollBack();
    print('Error:' .$e->getMessage());
    $errors['error'] = "データベース接続失敗しました。";

}

  // エラーセッション解除
  $_SESSION["errors"] = array();


}


//
// Twig
//

// Composerで作成されたautoload.phpを読み込む
require_once('../vendor/autoload.php');
// Twig_Loader_Filesystem と Twig instance の生成を読み込む
require_once('../config/twig.php');

// render
echo $twig->render('nation_registration_complate.html' , array(

  'nation' => $nation,
  'reason' => $reason

  )
);
