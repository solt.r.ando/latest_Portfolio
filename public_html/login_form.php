<?php

require_once('./functions.php');
require_once('../config/config.php');

// ログインしていれば TOPページ  に遷移
require_unlogined_session();

$errors = '';
$email = '';
$password = '';
$email_errors = '';
$password_errors = '';
$loginErrors = '';

// Cookie情報の取得

// メールアドレスのログインエラー
if (isset($_SESSION['email_errors'])) {

  $email_errors = $_SESSION['email_errors'];

}

// パスワードのログインエラー
if (isset($_SESSION['password_errors'])) {

  $password_errors = $_SESSION['password_errors'];

}


if (isset($_SESSION['not_match'])) {

  $loginErrors = $_SESSION['not_match'];

}






if (isset($_SESSION["email"])) {

  $email = $_SESSION["email"];

}

if (isset($_SESSION["password"])) {

  $password = $_SESSION["password"];

}



// クロスサイトリクエストフォージュリ(CSRF)対策・32バイトのCSRFトークンを作成
$TOKEN_LENGTH = 16; // 16*2 = 32バイト
$tokenByte = openssl_random_pseudo_bytes($TOKEN_LENGTH);
$csrfToken = bin2hex($tokenByte);
$_SESSION["csrfToken"] = $csrfToken;

// クリックジャッキング対策
header("X-FRAME-OPTIONS: SAMEORIGIN");



//
// Twig
//

// Composerで作成されたautoload.phpを読み込む
require_once('../vendor/autoload.php');
// Twig_Loader_Filesystem と Twig instance の生成を読み込む
require_once('../config/twig.php');

// render
echo $twig->render('login_form.html', array(

  'email' => $email,
  'password' => $password,
  'errors' => $errors,
  'email_errors' => $email_errors,
  'password_errors' => $password_errors,
  'loginErrors' => $loginErrors

  )
);
