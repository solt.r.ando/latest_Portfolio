<?php

require_once('./functions.php');
require_once('../config/config.php');
// Composerで作成されたautoload.phpを読み込む
require_once('../vendor/autoload.php');

//　MyValidation クラスのオブジェクト(インスタンス)の生成
$Nation = new lib\Controller\Nation();

$Nation->run();


// POST時
if (filter_input(INPUT_SERVER, "REQUEST_METHOD") == "POST") {

    $_SESSION["nation"] = filter_input(INPUT_POST, 'nation');
    $_SESSION["reason"] = filter_input(INPUT_POST, 'reason');


    $nation = $_SESSION["nation"];
    $reason = $_SESSION["reason"];


    //
    // 未入力チェック
    //


    /*
      バリデーション
      MyValidationクラスのインスタンスの生成
    */


    $check_result = [];

    // 必須項目のバリデーション
    $check_result = $Nation->checkNation('国名', $nation);
    $check_result = $Nation->checkReason('理由', $reason);


    if (!empty($check_result)) {

      $_SESSION['check_result'] = $check_result;
      header('Location: nation_registration_form.php');

    }


}



//
// Twig
//

// Twig_Loader_Filesystem と Twig instance の生成を読み込む
require_once('../config/twig.php');

// render
echo $twig->render('nation_registration_confirm.html', array(

  'nation' => $nation,
  'reason' => $reason,
  'check_result' => $check_result



  )
);
