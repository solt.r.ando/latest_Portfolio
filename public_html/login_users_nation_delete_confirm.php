<?php

require_once('./functions.php');
require_once('../config/db.php');
require_once('../config/config.php');

// ログインしていなければ login_form.php に遷移
require_logined_session();

// POST時
if (filter_input(INPUT_SERVER, 'REQUEST_METHOD') == 'POST') {

// セッション変数の定義
$_SESSION["id"] = filter_input(INPUT_POST, 'id');
$_SESSION["nation"] = filter_input(INPUT_POST, 'nation');
$_SESSION["reason"] = filter_input(INPUT_POST, 'reason');

// 変数の定義
$id = $_SESSION["id"];
$nation = $_SESSION["nation"];
$reason = $_SESSION["reason"];

}


//
// Twig
//

// Composerで作成されたautoload.phpを読み込む
require_once('../vendor/autoload.php');
// Twig_Loader_Filesystem と Twig instance の生成を読み込む
require_once('../config/twig.php');

// render
echo $twig->render('login_users_nation_delete_confirm.html', array(

  'id' => $id,
  'nation' => $nation,
  'reason' => $reason

  )
);
