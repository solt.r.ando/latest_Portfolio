<?php

require_once('../config/config.php');

// エラーメッセージの初期化
$errors = array();
$e = '';
// 配列の初期化
$rows = array();




require_once("../config/db.php");


try {

    //例外処理を投げる（スロー）ようにする
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $statement = $dbh->prepare('SELECT * FROM country WHERE nation LIKE :search or reason like :search');

    if ($statement) {

        $search = filter_input(INPUT_POST, "search");
        $like_search = "%$search%";

        // プレースホルダーへ実際の値を設定する
        $statement->bindValue(":search", $like_search, PDO::PARAM_STR);


        if ($statement->execute()) {
            // レコード件数取得
            $row_count = $statement->rowCount();

            while ($row = $statement->fetch()) {
                $rows[] = $row;
            }
        } else {
            $errors["error"] = "検索失敗しました。";
        }

        // データベース切断
      //  $dbh = null;
    }
} catch (PDOexception $e) {
    print('Error:'.$e->getMessage());
    $errors["error"] = "データベース接続失敗しました。";
}




//
// Twig
//

// Composerで作成されたautoload.phpを読み込む
require_once('../vendor/autoload.php');
// Twig_Loader_Filesystem と Twig instance の生成を読み込む
require_once('../config/twig.php');

// render
echo $twig->render('search.html', array(

  'errors' => $errors,
  'rows' => $rows,
  'dbh' => $dbh,
  'statement' => $statement,
  'search' => $search,
  'like_search' => $like_search,
  'row_count' => $row_count,
  'e' => $e,


  )
);
