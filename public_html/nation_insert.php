<?php
ini_set("display_errors", 1);
error_reporting(E_ALL);

session_start();

// 文字化け対策
header("Content-type: text/html; charset=UTF-8");

require_once('./functions.php');
require_once("../config/db.php");

// ログインしていなければ login_form.php に遷移
require_logined_session();


// POST時
if (filter_input(INPUT_SERVER, "REQUEST_METHOD") == "POST") {


  $rank = $_SESSION["rank"];
  $nation = $_SESSION["nation"];
  $food = $_SESSION["food"];
  $reason = $_SESSION["reason"];




    try {

      $dbh = new PDO($dsn, $user, $password);

      // プリペアドステートメント
      $statement = $dbh->prepare("INSERT INTO country (rank, nation, food, reason)
      VALUES (:rank, :nation, :food, :reason)");

      // トランザクション開始
      $dbh->beginTransaction();


        if ($statement) {

          // プレースホルダへ実際の値を設定する
          $statement->bindValue(":rank", $rank, PDO::PARAM_STR);
          $statement->bindValue(":nation", $nation, PDO::PARAM_STR);
          $statement->bindValue(":food", $food,  PDO::PARAM_STR);
          $statement->bindValue(":reason", $reason, PDO::PARAM_STR);

          if (!$statement->execute()) {

            $errors["error"] = "データベース接続失敗しました。";

        }

          // トランザクションコミット
          $dbh->commit();

    }

} catch(PDOException $e) {

    $dbh->rollBack();
    print('Error:' .$e->getMessage());
    $errors['error'] = "データベース接続失敗しました。";

}

  // エラーセッション解除
  $_SESSION["errorMsg"] = array();


}


//
// Twig
//

// Composerで作成されたautoload.phpを読み込む
require_once('../vendor/autoload.php');
// Twig_Loader_Filesystemを使う。
$loader = new Twig_Loader_Filesystem('../templates');
// $loaderをTwigの環境設定として Twig instance を生成
$twig = new Twig_Environment($loader);

// render
echo $twig->render('nation_insert.html' , array(

  'rank' => $rank,
  'nation' => $nation,
  'food' => $food,
  'reason' => $reason

  )
);
