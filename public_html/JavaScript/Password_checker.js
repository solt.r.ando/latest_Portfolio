$(function() {


  // 新規登録フォームを開いたときにデフォルトでボタンを押せないように設定
  if ($('#email').val() == '' && $('#password_id').val() == '') {

    $('#confirm_btn').prop('disabled', true)

  }



  // 氏名のバリデーション
  $('#name').blur(function() {

    if ($('#name').val() == '') {

      $('#js_NameError').text('氏名を入力してください。').css('color', 'red')

    } else {

      $('#js_NameError').text('')

    }

    // 必須項目が埋まっていたらボタンが押せる
    if ($('#name').val() != '' && $('#email').val() != '' &&
      $('#password_id').val().length > 7 && $('#tel').val() != '') {

      $('#confirm_btn').prop('disabled', false)

    }
  });



  // メールアドレスのバリデーション
  $('#email').blur(function() {

    //もしテキストボックスが空だったら
    if ($('#email').val() == '') {

      //隣の要素に警告文を表示する
      $('#js_EmailError').text('メールアドレスを入力してください。').css('color', 'red')
      $('#confirm_btn').prop('disabled', true)


    } else {

      //空じゃなかったら、隣の要素の警告文を消す
      $('#js_EmailError').text('')

    }


    // メールアドレスの重複のバリデーション
    if ($('#email').val() != '') {

      $.post('./Checker/Email_checker.php', {

        email: $('#email').val()

      }, function(data) {

        $('#js_EmailError').text(data.duplication)

      })
    }


    // 必須項目が埋まっていたらボタンが押せる
    if ($('#name').val() != '' && $('#email').val() != '' &&
      $('#password_id').val().length > 7 && $('#tel').val() != '') {

      $('#confirm_btn').prop('disabled', false)

    }
  });


  // パスワードのバリデーション
  $('#password_id').blur(function() {

    //もしテキストボックスが空だったら
    if ($('#password_id').val() == '') {

      //隣の要素に警告文を表示する
      $('#js_PasswordError').text('パスワードを入力してください。').css('color', 'red')
      $('#confirm_btn').prop('disabled', true)

      //テキストボックスに文字が入力されたら
    } else if ($('#password_id').val().length > 0 && $('#password_id').val().length < 8) {

      $('#js_PasswordError').text('パスワードは8文字以上で入力してください。').css('color', 'red')
      $('#confirm_btn').prop('disabled', true)

    } else {

      //隣の要素の警告文を消す
      $('#js_PasswordError').text('')

    }


    // 必須項目が埋まっていたらボタンが押せる
    if ($('#name').val() != '' && $('#email').val() != '' &&
      $('#password_id').val().length > 7 && $('#tel').val() != '') {

      $('#confirm_btn').prop('disabled', false)

    }
  });



  // 電話番号のバリデーション
  $('#tel').blur(function() {

    if ($('#tel').val() == '') {

      $('#js_TelError').text('電話番号を入力してください。').css('color', 'red')

    } else {

      $('#js_TelError').text('')

    }

    //　電話番号の文字数
    // TODO:


    // ハイフンがなければ
    // TODO:


    // 必須項目が埋まっていたらボタンが押せる
    if ($('#name').val() != '' && $('#email').val() != '' &&
      $('#password_id').val().length > 7 && $('#tel').val() != '') {

      $('#confirm_btn').prop('disabled', false)

    }

  });



  // 都道府県のバリデーション
  // TODO:





});
