<?php

require_once('../config/config.php');

$errors = '';

require_once('./functions.php');
require_once('../config/db.php');

// ログインしていなければ login_form.php に遷移
require_logined_session();


// セッション変数の定義
$id = $_SESSION["id"];



try {

    $dbh = new PDO($dsn, $user, $password);

    // プリペアドステートメント
    $statement = $dbh->prepare("DELETE from country where id = :id");

    // トランザクション開始
    $dbh->beginTransaction();

    if ($statement) {
        // プレースホルダーへ実際の値を設定する
        $statement->bindValue(':id', $id, PDO::PARAM_STR);

        //　実行する
        $statement->execute();

        // トランザクションコミット
        $dbh->commit();


    }

} catch (PDOException $e) {

    $dbh->rollBack();
    print('Error:' .$e->getMessage());
    $errors["error"] = "データベース接続失敗しました。";

}


//
// Twig
//

// Composerで作成されたautoload.phpを読み込む
require_once('../vendor/autoload.php');
// Twig_Loader_Filesystem と Twig instance の生成を読み込む
require_once('../config/twig.php');

// render
echo $twig->render('nation_delete_end.html', array(

  'id' => $id,
  'errors' => $errors

  )
);
