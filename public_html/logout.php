<?php

require_once('./functions.php');
require_once('../config/db.php');
require_once('../config/config.php');

// ログインしていなければ login_form.php に遷移
require_logined_session();

// セッション変数を全て解除
$_SESSION = array();

// セッションクッキーを削除
setcookie("PHPSESSID", '', time() - 1800, '/');


// セッションを破棄する
session_destroy();


//例外処理を投げる（スロー）ようにする
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$sql = "SELECT users.name, nation, reason, country.creation_time
        FROM country JOIN users ON country.userId = users.userId";

$statement = $dbh->query($sql);

// 結果の取得
$country = array();

foreach ($statement as $row) {

    $country[] = $row;

}



// ログインしなかった人が投稿した情報

$nologin_users = "匿名さん";

//例外処理を投げる（スロー）ようにする
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$sql = "SELECT nation, reason, creation_time
        FROM country
        WHERE country.userId is NULL";

$statement = $dbh->query($sql);

// 結果の取得
$countries = array();

foreach ($statement as $value) {

    $countries[] = $value;

}

// TOP画面にリダイレクト
header('Location: ' . SITE_URL . '/index.php');





//
// Twig
//

// Composerで作成されたautoload.phpを読み込む
require_once ('../vendor/autoload.php');
// Twig_Loader_Filesystem と Twig instance の生成を読み込む
require_once('../config/twig.php');

// Render
echo $twig->render('logout.html',array(

  'country' => $country,
  'countries' => $countries,
  'value' => $value,
  'nologin_users' => $nologin_users

  )
);
