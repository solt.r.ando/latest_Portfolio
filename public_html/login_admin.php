<?php

require_once('./functions.php');
require_once('../config/db.php');
require_once('../config/config.php');

// ログインしていなければ login_form.php へ遷移
require_logined_session();

// ログイン状態情報を Cookie に保存
// TODO: 


//
// ユーザー名の取得
//

try {


//例外処理を投げる（スロー）ようにする
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$sql = "SELECT name FROM users where userId = '$_SESSION[userId]'";

$statement = $dbh->query($sql);

// 結果の取得
$country = array();

foreach ($statement as $row) {

    $name = $row['name'];


}

} catch(PDOException $e) {

  print('Error:'.$e->getMessage());

}




//
// 行ってみたい国のテーブル情報の取得(ログインユーザー)
//


try {


//例外処理を投げる（スロー）ようにする
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// usersテーブルと countryテーブルの結合
$sql = "SELECT id, users.name, nation, reason, country.creation_time
        FROM country JOIN users ON country.userId = users.userId";


$statement = $dbh->query($sql);

// 結果の取得
$country = array();

foreach ($statement as $row) {

    $country[] = $row;

    $_SESSION['nation'] = $row['nation'];
    $_SESSION['reason'] = $row['reason'];
    $_SESSION['id'] = $row['id'];

}

} catch(PDOException $e) {

  print('Error:'.$e->getMessage());

}




// ログインしていないユーザーが投稿した情報

$nologin_users = "匿名さん";

//例外処理を投げる（スロー）ようにする
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$sql = "SELECT id, nation, reason, creation_time
        FROM country
        WHERE userId is NULL";

$statement = $dbh->query($sql);

// 結果の取得
$countries = array();

foreach ($statement as $value) {

    $countries[] = $value;

    $_SESSION['id'] = $value['id'];

}

//　ユーザー情報のセッションの初期化
$_SESSION['name'] = '';
$_SESSION['email'] = '';
$_SESSION['tel'] = '';
$_SESSION['errors'] = '';

// 国の登録のセッションの初期化
$_SESSION['nation'] = '';
$_SESSION['reason'] = '';



//
// Twig
//

// Composerで作成されたautoload.phpを読み込む
require_once ('../vendor/autoload.php');
// Twig_Loader_Filesystem と Twig instance の生成を読み込む
require_once('../config/twig.php');

// Render
echo $twig->render('login_admin.html',array(

  'country' => $country,
  'name' => $name,
  'countries' => $countries,
  'value' => $value,
  'nologin_users' => $nologin_users

  )
);
