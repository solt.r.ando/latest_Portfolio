<?php

require_once('../config/config.php');

// 変数の初期化
$errors = "";
$nation = '';
$reason = '';

//　バリデーションエラー
if (isset($_SESSION["errors"])) {

    $errors = $_SESSION["errors"];

}


if (isset($_SESSION['nation'])) {

  $nation = $_SESSION['nation'];

}

if (isset($_SESSION['reason'])) {

  $reason = $_SESSION['reason'];

}





//
// Twig
//

// Composerで作成されたautoload.phpを読み込む
require_once('../vendor/autoload.php');
// Twig_Loader_Filesystem と Twig instance の生成を読み込む
require_once('../config/twig.php');

// render
echo $twig->render('login_users_nation_registration_form.html', array(

  'errors' => $errors,

  'nation' => $nation,
  'reason' => $reason


  )
);
