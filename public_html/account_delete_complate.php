<?php

$errors = '';

require_once('./functions.php');
require_once('../config/db.php');
require_once('../config/config.php');

// ログインしていなければ login_form.php に遷移
require_logined_session();

// セッション変数の定義
$userId = $_SESSION['userId'];


try {

    // プリペアドステートメント
    $statement = $dbh->prepare("DELETE from users where userId = :userId");

    // トランザクション開始
    $dbh->beginTransaction();

    if ($statement) {
        // プレースホルダーへ実際の値を設定する
        $statement->bindValue(':userId', $userId, PDO::PARAM_STR);

        //　実行する
        $statement->execute();

        // トランザクションコミット
        $dbh->commit();


    }

} catch (PDOException $e) {

    print('Error:' .$e->getMessage());
    $errors["error"] = "データベース接続失敗しました。";

}



//
// Twig
//

// Composerで作成されたautoload.phpを読み込む
require_once('../vendor/autoload.php');
// Twig_Loader_Filesystem と Twig instance の生成を読み込む
require_once('../config/twig.php');

// render
echo $twig->render('account_delete_complate.html', array(

  'userId' => $userId,

  'errors' => $errors



  )
);
