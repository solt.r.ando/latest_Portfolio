<?php
require_once('../config/config.php');

/*
  ログイン状態によってリダイレクトを行うsession_startのラッパー関数
  初回時または失敗時にはヘッダを送信してexitする
*/

function require_unlogined_session() {

  // セッション開始
  @session_start();
  // ログインしていれば TOPページ  に遷移
  if (isset($_SESSION['userId'])) {

    header('Location:' . SITE_URL . '/login_users.php');
    exit;

  }
}


function require_logined_session() {

  // セッション開始
  @session_start();
  // ログインしていなければ login_form.php に遷移
  if (!isset($_SESSION['userId'])) {

    header('Location:' . SITE_URL . '/login_form.php');
    exit;

  }
}


/*
  CSRFトークンの生成

  @return string トークン

*/

function generate_token() {

  // セッションIDからハッシュを生成
  return hash('sha256', session_id());

}


/*
  CSRFトークンの生成

  @param string $token
  @return bool 検証結果
*/

function validate_token($token) {

  //  送信されてきたトークンがこちらで生成したハッシュと一致するか検証

  return $token = generate_token();

}





 ?>
