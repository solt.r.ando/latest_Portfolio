<?php

require_once('./functions.php');
require_once('../config/config.php');



// エラーメッセージの配列の初期化
$errors = array();




// POST時
if (filter_input(INPUT_SERVER, "REQUEST_METHOD") == "POST") {

    $_SESSION["nation"] = filter_input(INPUT_POST, 'nation');
    $_SESSION["reason"] = filter_input(INPUT_POST, 'reason');


    $nation = $_SESSION["nation"];
    $reason = $_SESSION["reason"];


    //
    // 未入力チェック
    //


    // 国名の未入力チェック
    if (empty($_SESSION["nation"])) {
        $errors[] = "国名を入力してください。";
    }


    // 理由の未入力チェック
    if (empty($_SESSION["reason"])) {
        $errors[] = "理由を入力してください。";
    }






    // エラーの数の判定
    if (count($errors) > 0) {

        $_SESSION["errors"] = $errors;
        header ("Location: nation_registration_form.php");

    } else {

      $_SESSION['errors'] = array();

    }


}



//
// Twig
//

// Composerで作成されたautoload.phpを読み込む
require_once('../vendor/autoload.php');
// Twig_Loader_Filesystem と Twig instance の生成を読み込む
require_once('../config/twig.php');

// render
echo $twig->render('login_users_nation_registration_confirm.html', array(

  'nation' => $nation,
  'reason' => $reason,
  'errors' => $errors,



  )
);
