<?php

        // 文字数が8文字より下だった場合
if (strlen($_POST['password']) > 0 && strlen($_POST['password']) < 8) {

    $response['length'] = "パスワードは8文字以上で入力してください。";

    header('Content-Type: application/json; charset=utf-8');
    echo json_encode($response);

}
