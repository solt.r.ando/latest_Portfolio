<?php
// 開発時にこれを書かないのはNG
ini_set('display_errors', TRUE);
error_reporting(E_ALL);

session_start();

//　文字化けを治す
header("Content-type: text/html; charset=utf-8");

// 変数の定義(初期化)
$name = "";
$email = "";
$password = "";
$tel = "";
$prefectures = "";
$hobby = "";
$gender = "";
$contact = "";
$string = "";


$date = date_default_timezone_set('Asia/Tokyo');


$form_Content = array($name, $email, $password, $tel, $prefectures, $hobby, $gender, $contact);

// エラーメッセージの配列の初期化
$errorMsg = array();
// セッション変数の初期化
$_SESSION['errorMsg'] = array();

// XSS対策 (hrefやsrcの値がURLか確認する)
  function urlCheck($form_Content) {

      if (!preg_match("/^(https?:.+)$/", $form_Content)) {

          return $errorMsg[] = "不正を検知しました。";

      } else {

          return true;

      }
  }



// POST時
if (filter_input(INPUT_SERVER, 'REQUEST_METHOD') == 'POST') {


    $_SESSION["name"] = filter_input(INPUT_POST, 'name');
    $_SESSION["email"] = filter_input(INPUT_POST, 'email');
    $_SESSION["password"] = filter_input(INPUT_POST, 'password');
    $_SESSION["tel"] = filter_input(INPUT_POST, 'tel');
    $_SESSION["prefectures"] = filter_input(INPUT_POST, 'prefectures');
    $_SESSION["hobby"] = filter_input(INPUT_POST, 'hobby', FILTER_DEFAULT,FILTER_REQUIRE_ARRAY);
    $_SESSION["gender"] = filter_input(INPUT_POST, 'gender');
    $_SESSION["contact"] = filter_input(INPUT_POST, 'contact');

    $name  = $_SESSION["name"];
    $email  = $_SESSION["email"];
    $password  = $_SESSION["password"];
    $tel  = $_SESSION["tel"];
    $prefectures  = $_SESSION["prefectures"];
    $hobby  = $_SESSION["hobby"];
    $gender = $_SESSION["gender"];
    $contact  = $_SESSION["contact"];



//
// 未入力チェック
//

// エラーメッセージの配列の初期化
$errorMsg = array();

// 名前の未入力チェック
if (empty($_SESSION["name"])) {
    $errorMsg[] = "名前を入力してください。";
}

// メールアドレスの未入力チェック
if (empty($_SESSION["email"])) {
    $errorMsg[] = "メールアドレスを入力してください。";
}

// パスワードの未入力チェック
if (empty($_SESSION["password"])) {
    $errorMsg[] = "パスワードを入力してください。";
}

// 電話番号の未入力チェック
if (empty($_SESSION["tel"])) {
    $errorMsg[] = "電話番号を入力してください。";
}

// 都道府県の未入力チェック
if ($_SESSION["prefectures"] == "選択") {
    $errorMsg[] = "都道府県を選択してください。";
}

  //
  // 文字数チェック
  //

  // 名前の文字数チェック
  if (strlen($_SESSION["name"]) >= 60) {
    $errorMsg[] = "氏名が長すぎます。";
  }

  // パスワード文字数チェック(8文字以上か)
  if (preg_match("/^[a-zA-Z1-9]{1,7}$/", $_SESSION["password"])) {
      $errorMsg[] = "パスワードは8文字以上で入力してください。";
  }

  //　電話番号の文字数チェック(10文字 or 11文字)
  if (strlen($_SESSION["tel"]) >= 1 && strlen($_SESSION["tel"]) <= 9 && preg_match("/^[0-9]+$/", $_SESSION["tel"])) {
      $errorMsg[] = "電話番号は10文字か11文字で入力してください。";
  } elseif (strlen($_SESSION["tel"]) >= 12 && preg_match("/^[0-9]+$/", $_SESSION["tel"])) {
      $errorMsg[] = "電話番号は10文字か11文字で入力してください。";
  }

  //
  // 形式チェック
  //

  // メールアドレス形式チェック
  if (!preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $_SESSION["email"]) && $_SESSION["email"] !== "") {
      $errorMsg[] = "メールアドレスに間違いがあります。";
  }

  // 電話番号の形式チェック
  if (preg_match("/[-]+/", $_SESSION["tel"])) {
      $errorMsg[] = "電話番号はハイフンなしで入力してください。";
  }

  //
  // 半角英数字チェック
  //

  //　電話番号の数字チェック
  if (!preg_match("/[0-9]/", $_SESSION["tel"]) && $_SESSION["tel"] !== "") {
    $errorMsg[] = "電話番号は半角数字で入力してください。";
  }


  // 電話番号の全角チェック Todo
  //if (strlen($tel) !== mb_strlen($tel, "UTF-8")) {
    //  $errorMsg[] = "電話番号に全角が含まれています。";
  //}


  if (count($errorMsg) >= 1) {
      $_SESSION['errorMsg'] = $errorMsg;

      header("location: registration_form.php");
      exit;

  } else {

    $errorMsg = array();

  }

}



//
// Twig
//

// Composerで作成されたautoload.phpを読み込む
require_once ('../vendor/autoload.php');
// Twig_loader_Filesystemを使う。confirm.phpからのtemplatesディレクトリを指定。(相対パス)
$loader = new Twig_Loader_filesystem('../templates');
// $loaderをTwigの環境設定として twig instance を生成
$twig = new Twig_Environment($loader);


// render
echo $twig->render('confirm.html', array (

   'name' => $name,
   'email' => $email,
   'password' => $password,
   'tel' => $tel,
   'prefectures' => $prefectures,
   'hobby' => $hobby,
   'gender' => $gender,
   'contact' => $contact,

   'errorMsg' => $errorMsg,
   'string' => $string


   )
);
