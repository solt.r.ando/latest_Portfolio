<?php
ini_set("display_errors", 1);
error_reporting(E_ALL);

session_start();

// 文字化け対策
header("Content-type: text/html; charset=UTF-8");

require_once('./functions.php');
require_once('../config/db.php');

// ログインしていなければ login_form.php に遷移
require_logined_session();

// 初期化
$errors = array();


// POST時
if (filter_input(INPUT_SERVER, 'REQUEST_METHOD') == 'POST') {


  $name = filter_input(INPUT_POST, 'name');
  $email = filter_input(INPUT_POST, 'email');
  $tel = filter_input(INPUT_POST, 'tel');

  $_SESSION['name'] = $name;
  $_SESSION['email'] = $email;
  $_SESSION ['tel'] = $tel;



    try {

	       //例外処理を投げる（スロー）ようにする
	       $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


        // プリペアドステートメント
        $statement = $dbh->prepare("UPDATE users SET name = :name, email = :email, tel = :tel
        WHERE userId = :userId");

        // トランザクション開始
        $dbh->beginTransaction();


        if ($statement) {


            // プレースホルダへ実際の値を設定する
            $statement->bindValue(':name', $name, PDO::PARAM_STR);
            $statement->bindValue(':email',  $email, PDO::PARAM_STR);
            $statement->bindValue(':tel',  $tel, PDO::PARAM_STR);
            $statement->bindValue(':userId',  $_SESSION['userId'], PDO::PARAM_STR);


            //　クエリ実行
            $statement->execute();

            // トランザクションコミット
            $dbh->commit();


        }

    } catch (PDOException $e) {

        print('Error:' .$e->getMessage());
        $errors["error"] = "データベース接続失敗しました。";

    }

    // エラーセッション解除
    $_SESSION["errors"]= array();


}




//
// Twig
//

// Composerで作成されたautoload.phpを読み込む
require_once('../vendor/autoload.php');
// Twig_Loader_Filesystemを使う。account_edit_insert.phpからのtemplatesディレクトリを指定。(相対パス)
$loader = new Twig_Loader_Filesystem('../templates');
// $loaderをTwigの環境設定としてTwig instance を生成
$twig = new Twig_Environment($loader);

// render
// render
echo $twig->render('account_edit_insert.html', array(

  'errors' => $errors,


  )
);
