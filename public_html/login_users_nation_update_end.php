<?php

require_once('./functions.php');
require_once('../config/db.php');
require_once('../config/config.php');

// ログインしていなければ login_form.php に遷移
require_logined_session();


// 初期化
$errors = array();


// POST時
if (filter_input(INPUT_SERVER, 'REQUEST_METHOD') == 'POST') {


    $_SESSION["nation"] = filter_input(INPUT_POST, 'nation');
    $_SESSION["reason"] = filter_input(INPUT_POST, 'reason');




if (empty($_SESSION["nation"])) {
    $errors[] = "国名が入力されていません。";
}

if (empty($_SESSION["reason"])) {
    $errors[] = "理由が入力されていません。";
}


if (count($errors) > 0) {

    $_SESSION["errors"] = $errors;
    header("location: nation_update.php");
}



if (count($errors) === 0) {

    try {

        $dbh = new PDO($dsn, $user, $password);

        // プリペアドステートメント
        $statement = $dbh->prepare("UPDATE country SET nation = :nation, reason = :reason
        WHERE id = :id");


        // トランザクション開始
        $dbh->beginTransaction();

        if ($statement) {

            // プレースホルダへ実際の値を設定する
            $statement->bindValue(':nation', $_SESSION["nation"], PDO::PARAM_STR);
            $statement->bindValue(':reason', $_SESSION["reason"], PDO::PARAM_STR);
            $statement->bindValue(':id', $_SESSION["id"], PDO::PARAM_STR);


            //　クエリ実行
            $statement->execute();

            // トランザクションコミット
            $dbh->commit();

        }

    } catch (PDOException $e) {

        $dbh->rollBack();
        print('Error:' .$e->getMessage());
        $errors["error"] = "データベース接続失敗しました。";

    }

    // エラーセッション解除
    $_SESSION["errors"]= array();

}


}




//
// Twig
//

// Composerで作成されたautoload.phpを読み込む
require_once('../vendor/autoload.php');
// Twig_Loader_Filesystem と Twig instance の生成を読み込む
require_once('../config/twig.php');

// render
// render
echo $twig->render('login_users_nation_update_end.html', array(

  'errors' => $errors,

  )
);
