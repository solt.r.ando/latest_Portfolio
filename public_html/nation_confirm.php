<?php
ini_set("display_errors", 1);
error_reporting(E_ALL);

session_start();

// 文字化け対策
header("Content-type: text/html; charset=UTF-8");

require_once('./functions.php');

// ログインしていなければ login_form.php に遷移
require_logined_session();


// エラーメッセージの配列の初期化
$errorMsg = array();




// POST時
if (filter_input(INPUT_SERVER, "REQUEST_METHOD") == "POST") {

    $_SESSION["rank"] = filter_input(INPUT_POST, 'rank');
    $_SESSION["nation"] = filter_input(INPUT_POST, 'nation');
    $_SESSION["food"] = filter_input(INPUT_POST, 'food');
    $_SESSION["reason"] = filter_input(INPUT_POST, 'reason');

    $rank = $_SESSION["rank"];
    $nation = $_SESSION["nation"];
    $food = $_SESSION["food"];
    $reason = $_SESSION["reason"];


    //
    // 未入力チェック
    //

    // 順位の未入力チェック
    if (empty($_SESSION["rank"])) {
        $errorMsg[] = "順位を入力してください。";
    }

    // 順位の形式チェック & 半角数字チェック
    if (!preg_match('/^[0-9]+/', $_SESSION["rank"]) && !empty($_SESSION["rank"])) {
        $errorMsg[] = "順位は半角数字で入力してください。";
    }

    // 国名の未入力チェック
    if (empty($_SESSION["nation"])) {
        $errorMsg[] = "国名を入力してください。";
    }

    // 食べたい食べ物の未入力チェック
    if (empty($_SESSION["food"])) {
        $errorMsg[] = "食べたい食べ物を入力してください。";
    }

    // 理由の未入力チェック
    if (empty($_SESSION["reason"])) {
        $errorMsg[] = "理由を入力してください。";
    }






    // エラーの数の判定
    if (count($errorMsg) > 0) {

        $_SESSION["errorMsg"] = $errorMsg;
        header ("Location: nation_registration_form.php");

    }


}



//
// Twig
//

// Composerで作成されたautoload.phpを読み込む
require_once('../vendor/autoload.php');
// Twig_Loader_Filesystemを使う。
$loader = new Twig_Loader_Filesystem('../templates');
// $loaderをTwigの環境設定として Twig instance を生成
$twig = new Twig_Environment($loader);

// render
echo $twig->render('nation_confirm.html', array(

  'rank' => $rank,
  'nation' => $nation,
  'food' => $food,
  'reason' => $reason,
  'errorMsg' => $errorMsg,



  )
);
