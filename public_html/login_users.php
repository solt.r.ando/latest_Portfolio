<?php

require_once('./functions.php');
require_once('../config/db.php');
require_once('../config/config.php');

// ログインしていなければ login_form.php へ遷移
require_logined_session();

// 管理者権限があれば　管理者画面へリダイレクト
if ($_SESSION['admin_role'] == 'Y') {

  header('Location: login_admin.php');

}

//
// ユーザー名の取得
//

try {


//例外処理を投げる（スロー）ようにする
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$sql = "SELECT name FROM users where userId = '$_SESSION[userId]'";

$statement = $dbh->query($sql);

foreach ($statement as $row) {

    $name = $row['name'];


}

} catch(PDOException $e) {

  print('Error:'.$e->getMessage());

}




//
// 行ってみたい国のテーブル情報の取得 (ログインユーザー)
//

$userId = $_SESSION['userId'];

try {


//例外処理を投げる（スロー）ようにする
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// usersテーブルと countryテーブルの結合
$sql = "SELECT id, users.userId, users.name, nation, reason, country.creation_time
        FROM country JOIN users ON country.userId = users.userId
        WHERE users.userId = $userId";


$statement = $dbh->query($sql);

foreach ($statement as $row) {

    $country[] = $row;

    $_SESSION['name'] = $row['name'];
    $_SESSION['nation'] = $row['nation'];
    $_SESSION['reason'] = $row['reason'];
    $_SESSION['creation_time'] = $row['creation_time'];

}

} catch(PDOException $e) {

  print('Error:'.$e->getMessage());

}





// 行ってみたい国のテーブル情報の取得 (ログインユーザー以外)


$userId = $_SESSION['userId'];

try {


//例外処理を投げる（スロー）ようにする
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// usersテーブルと countryテーブルの結合
$sql = "SELECT users.userId, users.name, nation, reason, country.creation_time
        FROM country JOIN users ON country.userId = users.userId
        WHERE users.userId != $userId";


$statement = $dbh->query($sql);

foreach ($statement as $value) {

    $countries[] = $value;

    $_SESSION['other_users_name'] = $value['name'];
    $_SESSION['other_users_nation'] = $value['nation'];
    $_SESSION['other_users_reason'] = $value['reason'];
    $_SESSION['other_users_creation_time'] = $value['creation_time'];

}

} catch(PDOException $e) {

  print('Error:'.$e->getMessage());

}





// ログインしていないユーザーが投稿した情報

$nologin_users = "匿名さん";

//例外処理を投げる（スロー）ようにする
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$sql = "SELECT nation, reason, creation_time
        FROM country
        WHERE userId is NULL";

$statement = $dbh->query($sql);

// 結果の取得
$nation = array();

foreach ($statement as $val) {

    $nation[] = $val;

}





//　ユーザー情報のセッションの初期化
$_SESSION['name'] = '';
$_SESSION['email'] = '';
$_SESSION['tel'] = '';
$_SESSION['errors'] = '';

// 国の登録のセッションの初期化
$_SESSION['rank'] = '';
$_SESSION['nation'] = '';
$_SESSION['food'] = '';
$_SESSION['reason'] = '';



//
// Twig
//

// Composerで作成されたautoload.phpを読み込む
require_once ('../vendor/autoload.php');
// Twig_Loader_Filesystem と Twig instance の生成を読み込む
require_once('../config/twig.php');

// Render
echo $twig->render('login_users.html',array(

  'countries' => $countries,
  'row' => $row,
  'value' => $value,
  'name' => $name,
  'nation' => $nation,
  'val' => $val,
  'nologin_users' => $nologin_users


  )
);
