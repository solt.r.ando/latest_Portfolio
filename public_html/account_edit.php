<?php

require_once('./functions.php');
require_once('../config/db.php');
require_once('../config/config.php');

// ログインしていなければ login_form.php へ遷移
require_logined_session();

$errors = '';
$admin_role = '';


// 編集対象のアカウントがログインしているアカウントと同一であるか
try {


  //例外処理を投げる（スロー）ようにする
  $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  $sql = "SELECT userId, admin_role FROM users where userId = '$_SESSION[userId]'";

  $statement = $dbh->query($sql);

  // 結果の取得
  $country = array();

  foreach ($statement as $row) {

    //　編集対象のuserId
    $userId = $row['userId'];
    $admin_role = $row['admin_role'];

    //　ログインしている userId と編集対象の userId が同じかどうか
    if (!$_SESSION['userId'] === $userId) {

      header('Location: login_admin.php');

    }

  }



} catch(PDOException $e) {

    print('Error:'.$e->getMessage());

  }




// バリデーションエラー
if (count(array($_SESSION['errors'])) > 0) {

      $errors = $_SESSION['errors'];

      $name = $_SESSION['name'];
      $email = $_SESSION['email'];
      $tel = $_SESSION['tel'];

  } else {

    $name = $_SESSION['name'];
    $email = $_SESSION['email'];
    $tel = $_SESSION['tel'];

  }



  //
  // ユーザー名の取得
  //

// もしフォームが空だったら
if (empty($name) && empty($email) && empty($tel) && empty($_SESSION['errors'])) {


    try {


      //例外処理を投げる（スロー）ようにする
      $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

      $sql = "SELECT name, email, tel FROM users where userId = '$_SESSION[userId]'";

      $statement = $dbh->query($sql);

      // 結果の取得
      $country = array();

      foreach ($statement as $row) {

        $name = $row['name'];
        $email = $row['email'];
        $tel = $row['tel'];

      }

    } catch(PDOException $e) {

        print('Error:'.$e->getMessage());

      }

}



//
// Twig
//

// Composerで作成されたautoload.phpを読み込む
require_once('../vendor/autoload.php');
// Twig_Loader_Filesystem と Twig instance の生成を読み込む
require_once('../config/twig.php');

// render
echo $twig->render('account_edit.html', array(

  'errors' => $errors,
  'name' => $name,
  'email' => $email,
  'tel' => $tel

  )
);
