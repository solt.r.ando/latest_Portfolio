<?php

require_once('./functions.php');
require_once("../config/db.php");
require_once('../config/config.php');

// ログインしていなければ login_form.php に遷移
require_logined_session();

$errors = '';
$id = '';
$nation = '';
$reason = '';

// POST時
if (filter_input(INPUT_SERVER, 'REQUEST_METHOD') == 'POST') {


  $_SESSION["nation"] = filter_input(INPUT_POST, 'nation');
  $_SESSION["reason"] = filter_input(INPUT_POST, 'reason');
  $_SESSION["id"] = filter_input(INPUT_POST, 'id');

  $nation = $_SESSION["nation"];
  $reason = $_SESSION["reason"];
  $id = $_SESSION["id"];


}



if (isset($_SESSION['errors'])) {

    if (count(array($_SESSION['errors'])) > 0) {

      $errors = $_SESSION["errors"];

    }
  }



  $nation = $_SESSION["nation"];
  $reason = $_SESSION["reason"];
  $id = $_SESSION["id"];




//
// Twig
//

// Composerで作成されたautoload.phpを読み込む
require_once('../vendor/autoload.php');
// Twig_Loader_Filesystem と Twig instance の生成を読み込む
require_once('../config/twig.php');

// render
echo $twig->render('login_users_nation_update.html', array(

  'id' => $id,
  'nation' => $nation,
  'reason' => $reason,

  'errors' => $errors

  )
);
