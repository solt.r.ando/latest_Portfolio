<?php

require_once('./functions.php');
require_once('../config/db.php');
require_once('../config/config.php');

// ログインしていなければ login_form.php に遷移
require_logined_session();

// 初期化
$errors = array();


// POST時
if (filter_input(INPUT_SERVER, 'REQUEST_METHOD') == 'POST') {


  $name = filter_input(INPUT_POST, 'name');
  $email = filter_input(INPUT_POST, 'email');
  $tel = filter_input(INPUT_POST, 'tel');

  $_SESSION['name'] = $name;
  $_SESSION['email'] = $email;
  $_SESSION ['tel'] = $tel;



    try {

	       //例外処理を投げる（スロー）ようにする
	       $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


        // プリペアドステートメント
        $statement = $dbh->prepare("UPDATE users SET name = :name, email = :email, tel = :tel
        WHERE userId = :userId");

        // トランザクション開始
        $dbh->beginTransaction();


        if ($statement) {


            // プレースホルダへ実際の値を設定する
            $statement->bindValue(':name', $name, PDO::PARAM_STR);
            $statement->bindValue(':email',  $email, PDO::PARAM_STR);
            $statement->bindValue(':tel',  $tel, PDO::PARAM_STR);
            $statement->bindValue(':userId',  $_SESSION['userId'], PDO::PARAM_STR);


            //　クエリ実行
            $statement->execute();

            // トランザクションコミット
            $dbh->commit();


        }

    } catch (PDOException $e) {

        print('Error:' .$e->getMessage());
        $errors["error"] = "データベース接続失敗しました。";

    }

    // エラーセッション解除
    $_SESSION["errors"]= array();


}




//
// Twig
//

// Composerで作成されたautoload.phpを読み込む
require_once('../vendor/autoload.php');
// Twig_Loader_Filesystem と Twig instance の生成を読み込む
require_once('../config/twig.php');

// render
// render
echo $twig->render('login_users_account_edit_update.html', array(

  'errors' => $errors,


  )
);
