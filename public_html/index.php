<?php

require_once('../config/config.php');
require_once('../config/db.php');

// 変数の初期化
$string = "";
$email = '';

// ログインした人が投稿した情報

//例外処理を投げる（スロー）ようにする
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$sql = "SELECT users.name, nation, reason, country.creation_time
        FROM country JOIN users ON country.userId = users.userId";

$statement = $dbh->query($sql);

foreach ($statement as $row) {

    $country[] = $row;

}



// ログインしていないユーザーが投稿した情報

$nologin_users = "匿名さん";

//例外処理を投げる（スロー）ようにする
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$sql = "SELECT nation, reason, creation_time
        FROM country
        WHERE country.userId is NULL";

$statement = $dbh->query($sql);

foreach ($statement as $value) {

    $countries[] = $value;

}

$_SESSION = array();


//
// Twig
//

// Composerで作成されたautoload.phpを読み込む
require_once ('../vendor/autoload.php');
// Twig_Loader_Filesystem と Twig instance の生成を読み込む
require_once('../config/twig.php');

// Render
echo $twig->render('index.html', array(

  'string' => $string,
  'country' => $country,
  'countries' => $countries,
  'value' => $value,
  'nologin_users' => $nologin_users,
  'email' => $email


  )
);
