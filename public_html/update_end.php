<?php
ini_set("display_errors", 1);
error_reporting(E_ALL);

session_start();

// 文字化け対策
header("Content-type: text/html; charset=UTF-8");

require_once('./functions.php');
require_once('../config/db.php');

// ログインしていなければ login_form.php に遷移
require_logined_session();


// 初期化
$errors = array();


// POST時
if (filter_input(INPUT_SERVER, 'REQUEST_METHOD') == 'POST') {


    $_SESSION["rank"] = filter_input(INPUT_POST, 'rank');
    $_SESSION["nation"] = filter_input(INPUT_POST, 'nation');
    $_SESSION["food"] = filter_input(INPUT_POST, 'food');
    $_SESSION["reason"] = filter_input(INPUT_POST, 'reason');


// 入力チェック
if (empty($_SESSION["rank"])) {
    $errors[] = "順位が入力されていません。";
}

if (empty($_SESSION["nation"])) {
    $errors[] = "国名が入力されていません。";
}

if (empty($_SESSION["food"])) {
    $errors[] = "食べ物が入力されていません。";
}

if (empty($_SESSION["reason"])) {
    $errors[] = "理由が入力されていません。";
}


if (count($errors) > 0) {

    $_SESSION["errors"] = $errors;
    header("location: update.php");
}



if (count($errors) === 0) {

    try {

        $dbh = new PDO($dsn, $user, $password);

        // プリペアドステートメント
        $statement = $dbh->prepare("UPDATE country SET rank = :rank, nation = :nation, food = :food, reason = :reason
        WHERE id = :id");


        // トランザクション開始
        $dbh->beginTransaction();

        if ($statement) {

            // プレースホルダへ実際の値を設定する
            $statement->bindValue(':rank', $_SESSION["rank"], PDO::PARAM_STR);
            $statement->bindValue(':nation', $_SESSION["nation"], PDO::PARAM_STR);
            $statement->bindValue(':food', $_SESSION["food"], PDO::PARAM_STR);
            $statement->bindValue(':reason', $_SESSION["reason"], PDO::PARAM_STR);
            $statement->bindValue(':id', $_SESSION["id"], PDO::PARAM_STR);


            //　クエリ実行
            $statement->execute();

            // トランザクションコミット
            $dbh->commit();

        }

    } catch (PDOException $e) {

        $dbh->rollBack();
        print('Error:' .$e->getMessage());
        $errors["error"] = "データベース接続失敗しました。";

    }

    // エラーセッション解除
    $_SESSION["errors"]= array();

}


}




//
// Twig
//

// Composerで作成されたautoload.phpを読み込む
require_once('../vendor/autoload.php');
// Twig_Loader_Filesystemを使う。update_end.phpからのtemplatesディレクトリを指定。(相対パス)
$loader = new Twig_Loader_Filesystem('../templates');
// $loaderをTwigの環境設定としてTwig instance を生成
$twig = new Twig_Environment($loader);

// render
// render
echo $twig->render('update_end.html', array(

  'errors' => $errors,

  )
);
