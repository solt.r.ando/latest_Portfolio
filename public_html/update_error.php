<?php
ini_set("display_errors", 1);
error_reporting(E_ALL);

session_start();

// 文字化け対策
header("Content-type: text/html; charset=UTF-8");

require_once('./functions.php');
require_once("../config/db.php");

// ログインしていなければ login_form.php に遷移
require_logined_session();



if (isset($_SESSION["errors"])) {

  $errors = $_SESSION["errors"];

}

$id = $_SESSION["id"];
$rank = $_SESSION["rank"];
$nation = $_SESSION["nation"];
$food = $_SESSION["food"];
$reason = $_SESSION["reason"];





// rank の入力データ保持
if (isset($_SESSION['rank'])) {

  if ($_SESSION['rank'] === $_SESSION['rank']) {
    $name = $_SESSION['rank'];


  }
}


// nation の入力データ保持
if (isset($_SESSION['nation'])) {

  if ($_SESSION['nation'] === $_SESSION['nation']) {
    $name = $_SESSION['nation'];

  }
}


// food の入力データ保持
if (isset($_SESSION['food'])) {

  if ($_SESSION['food'] === $_SESSION['food']) {
    $name = $_SESSION['food'];

  }
}


// reason の入力データ保持
if (isset($_SESSION['reason'])) {

  if ($_SESSION['reason'] === $_SESSION['reason']) {
    $name = $_SESSION['reason'];

  }
}


// 入力チェック
if (empty($_SESSION["rank"])) {
    $errors[] = "順位が入力されていません。";
}

if (empty($_SESSION["nation"])) {
    $errors[] = "国名が入力されていません。";
}

if (empty($_SESSION["food"])) {
    $errors[] = "食べ物が入力されていません。";
}

if (empty($_SESSION["reason"])) {
    $errors[] = "理由が入力されていません。";
}


if (count($errors) > 0) {

    $_SESSION["errors"] = $errors;
    $errors = $_SESSION["errors"];

} 





//
// Twig
//

// Composerで作成されたautoload.phpを読み込む
require_once('../vendor/autoload.php');
// Twig_Loader_Filesystemを使う。update_error.phpからのtemplatesディレクトリを指定。(相対パス)
$loader = new Twig_Loader_Filesystem('../templates');
// $loaderをTwigの環境設定として Tiwg instance を生成
$twig = new Twig_Environment($loader);

// render
echo $twig->render('update_error.html', array(

  'id' => $id,
  'rank' => $rank,
  'nation' => $nation,
  'food' => $food,
  'reason' => $reason,

  'errors' => $errors

  )
);
