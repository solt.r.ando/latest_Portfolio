<?php

require_once('./functions.php');
require_once('../config/config.php');

// ログインしていなければ login_form.php に遷移
require_logined_session();




// 初期化
$errors = array();

// POST時
if (filter_input(INPUT_SERVER, 'REQUEST_METHOD') == 'POST') {

    $name = filter_input(INPUT_POST, 'name');
    $email = filter_input(INPUT_POST, 'email');
    $tel = filter_input(INPUT_POST, 'tel');


    // 入力チェック
    if (empty($name)) {
        $errors[] = "名前が入力されていません。";
    }

    if (empty($email)) {
        $errors[] = "E-mailが入力されていません。";
    }

    if (empty($tel)) {
        $errors[] = "電話番号が入力されていません。";
    }


    // 名前の文字数チェック
    if (strlen($name) >= 60) {
        $errors[] = "氏名が長すぎます。";
    }


    // 電話番号の文字数チェック(10文字 or 11文字)
    if (strlen($tel) >= 1 && strlen($tel) <= 9 && preg_match("/^[0-9]+$/", $tel)) {
        $errors[] = "電話番号は10文字か11文字で入力してください。";
    } elseif (strlen($tel) >= 12 && preg_match("/^[0-9]+$/", $tel)) {
        $errors[] = "電話番号は10文字か11文字で入力してください。";
    }

    // メールアドレス形式チェック
    if (!preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $email) && $email !== "") {
        $errors[] = "メールアドレスに間違いがあります。";
    }

    // 電話番号の形式チェック
    if (preg_match("/[-]+/", $tel)) {
        $errors[] = "電話番号はハイフンなしで入力してください。";
    }

    //　電話番号の半角チェック
    if (preg_match("/[a-zA-Z]/", $tel) && $tel !== "") {
        $errors[] = "電話番号は半角数字で入力してください。";
    }



    if (count($errors) > 0) {

        $_SESSION["errors"] = $errors;

        $_SESSION['name'] = $name;
        $_SESSION['email'] = $email;
        $_SESSION['tel'] = $tel;

        header("Location: login_users_account_edit.php");

    } else {

        $_SESSION['errors'] = array();

        $_SESSION['name'] = $name;
        $_SESSION['email'] = $email;
        $_SESSION['tel'] = $tel;

    }
}




//
// Twig
//


// Composer で作成されたautoload.phpを読み込む
require_once('../vendor/autoload.php');
// Twig_Loader_Filesystem と Twig instance の生成を読み込む
require_once('../config/twig.php');

// render
echo $twig->render('login_users_account_edit_confirm.html', array(

  'errors' => $errors,

  'name' => $name,
  'email' => $email,
  'tel' => $tel

  )
);
