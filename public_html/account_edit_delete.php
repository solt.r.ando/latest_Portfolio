<?php
// 開発時にこれを書かないのはNG
ini_set('display_errors', true);
error_reporting(E_ALL);

session_start();
header("Content-type: text/html; charset=utf-8");

require_once('./functions.php');
require_once('../config/db.php');

// ログインしていなければ login_form.php へ遷移
require_logined_session();

$errors = '';



// バリデーションエラー
if (count(array($_SESSION['errors'])) > 0) {

      $errors = $_SESSION['errors'];

      $name = $_SESSION['name'];
      $email = $_SESSION['email'];
      $tel = $_SESSION['tel'];

  } else {

    $name = $_SESSION['name'];
    $email = $_SESSION['email'];
    $tel = $_SESSION['tel'];

  }



  //
  // ユーザー名の取得
  //

// もしフォームが空だったら
if (empty($name) && empty($email) && empty($tel) && empty($_SESSION['errors'])) {


    try {


      //例外処理を投げる（スロー）ようにする
      $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

      $sql = "SELECT name, email, tel FROM users where userId = '$_SESSION[userId]'";

      $statement = $dbh->query($sql);

      // 結果の取得
      $country = array();

      foreach ($statement as $row) {

        $name = $row['name'];
        $email = $row['email'];
        $tel = $row['tel'];

      }

    } catch(PDOException $e) {

        print('Error:'.$e->getMessage());

      }

}



//
// Twig
//

// Composerで作成されたautoload.phpを読み込む
require_once('../vendor/autoload.php');
// Twig_Loader_Filesystemを使う。account_edit_delete.phpからのtemplatesディレクトリを指定(相対パス)
$loader = new Twig_Loader_Filesystem('../templates');
// $loaderをTwigの環境設定として tiwg instance を生成
$twig = new Twig_Environment($loader);

// render
echo $twig->render('account_edit_delete.html', array(

  'errors' => $errors,

  'name' => $name,
  'email' => $email,
  'tel' => $tel

  )
);
