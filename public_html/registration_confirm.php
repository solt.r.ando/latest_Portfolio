<?php

require_once('../config/config.php');
// Composerで作成されたautoload.phpを読み込む
require_once('../vendor/autoload.php');

// オブジェクトの作成
$app = new lib\Controller\Signup();
$app->run();




// セッションを変数に代入
$name = $_SESSION['name'];
$email = $_SESSION['email'];
$password = $_SESSION['password'];
$tel = $_SESSION['tel'];
$prefectures = $_SESSION['prefectures'];
$hobby = $_SESSION['hobby'];
$gender = $_SESSION['gender'];
$contact = $_SESSION['contact'];



//
// Twig
//


// Twig_Loader_Filesystem と Twig instance の生成を読み込む
require_once('../config/twig.php');


// render
echo $twig->render('registration_confirm.html', array (

   'name' => $name,
   'email' => $email,
   'password' => $password,
   'tel' => $tel,
   'prefectures' => $prefectures,
   'hobby' => $hobby,
   'gender' => $gender,
   'contact' => $contact


   )
);
