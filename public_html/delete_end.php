<?php
ini_set("display_errors", 1);
error_reporting(E_ALL);

session_start();

// 文字化け対策
header("Content-type: text/html; charset=UTF-8");

$errors = '';

require_once('./functions.php');
require_once('../config/db.php');

// ログインしていなければ login_form.php に遷移
require_logined_session();


// セッション変数の定義
$id = filter_input(INPUT_POST, 'id');



try {

    $dbh = new PDO($dsn, $user, $password);

    // プリペアドステートメント
    $statement = $dbh->prepare("DELETE from country where id = :id");

    // トランザクション開始
    $dbh->beginTransaction();

    if ($statement) {
        // プレースホルダーへ実際の値を設定する
        $statement->bindValue(':id', $id, PDO::PARAM_STR);

        //　実行する
        $statement->execute();

        // トランザクションコミット
        $dbh->commit();


    }

} catch (PDOException $e) {

    $dbh->rollBack();
    print('Error:' .$e->getMessage());
    $errors["error"] = "データベース接続失敗しました。";

}



//
// Twig
//

// Composerで作成されたautoload.phpを読み込む
require_once('../vendor/autoload.php');
// Twig_Loader_Filesystemを使う。dalete_end.phhpからのtemplatesディレクトリを指定。(相対パス)
$loader = new Twig_Loader_Filesystem('../templates');
// $loaderをTwigの環境設定として Twig instance を生成
$twig = new Twig_Environment($loader);

// render
echo $twig->render('delete_end.html', array(

  'id' => $id,

  'errors' => $errors



  )
);
