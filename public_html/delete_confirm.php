<?php
ini_set("display_errors", 1);
error_reporting(E_ALL);

session_start();

// 文字化け対策
header("Content-type: text/html; charset=UTF-8");

require_once('./functions.php');
require_once('../config/db.php');

// ログインしていなければ login_form.php に遷移
require_logined_session();


// セッション変数の定義
$_SESSION["id"] = filter_input(INPUT_POST, 'id');
$_SESSION["rank"] = filter_input(INPUT_POST, 'rank');
$_SESSION["nation"] = filter_input(INPUT_POST, 'nation');
$_SESSION["food"] = filter_input(INPUT_POST, 'food');
$_SESSION["reason"] = filter_input(INPUT_POST, 'reason');

// 変数の定義
$id = $_SESSION["id"];
$rank = $_SESSION["rank"];
$nation = $_SESSION["nation"];
$food = $_SESSION["food"];
$reason = $_SESSION["reason"];



//
// Twig
//

// Composerで作成されたautoload.phpを読み込む
require_once('../vendor/autoload.php');
// Twig_Loader_Filesystemを使う。delete_confirm.phpからのtemplatesディレクトリを指定。(相対パス)
$loader = new Twig_Loader_Filesystem('../templates');
// $loaderをTwigの環境設定としてTwig instance を生成
$twig = new Twig_Environment($loader);

// render
echo $twig->render('delete_confirm.html', array(

  'id' => $id,
  'rank' => $rank,
  'nation' => $nation,
  'food' => $food,
  'reason' => $reason

  )
);
