<?php
require_once('../config/config.php');
// Composerで作成されたautoload.phpを読み込む
require_once('../vendor/autoload.php');

// オブジェクトの作成




  // encodeされたデータを元に戻す
  // $js_error = json_decode($password_json, true);
  // $js_error_message = $js_error['message'];





$name_errors = '';
$email_errors = '';
$password_errors = '';
$tel_errors = '';
$prefectures_errors = '';
$token = '';


if (isset($_SESSION['token'])) {

  $token = $_SESSION['token'];

}


// 名前のバリデーションエラーメッセージ
if (isset($_SESSION['name_errors'])) {

  $name_errors = $_SESSION['name_errors'];

}

// Eメールのバリデーションエラーメッセージ
if (isset($_SESSION['email_errors'])) {

  $email_errors = $_SESSION['email_errors'];

}

// パスワードのバリデーションエラーメッセージ
if (isset($_SESSION['password_errors'])) {

  $password_errors = $_SESSION['password_errors'];

}

// 電話番号のバリデーションエラーメッセージ
if (isset($_SESSION['tel_errors'])) {

  $tel_errors = $_SESSION['tel_errors'];

}

// 都道府県のバリデーションエラーメッセージ
if (isset($_SESSION['prefectures_errors'])) {

  $prefectures_errors = $_SESSION['prefectures_errors'];

}





if (isset($_SESSION['token'])) {

  $token = $_SESSION['token'];

}

// 変数の定義(初期化)
$name = "";
$email = "";
$password = "";
$tel = "";
$prefectures = "";
$hobby = "";
$gender = "";
$contact = "";
$check_result = "";



// バリデーションエラーのセッションの受け取り
if (isset($_SESSION['check_result'])) {

  $check_result = $_SESSION['check_result'];


}



//
// 確認フォームから戻った時のデータ保持
//

// name の入力データ保持
if (isset($_SESSION['name'])) {

    $name = $_SESSION['name'];

}

//email の入力データの保持
if (isset($_SESSION['email'])) {

    $email = $_SESSION['email'];

  }


//password の入力データの保持
if (isset($_SESSION['password'])) {

    $password = $_SESSION['password'];

}


//tel の入力データの保持
if (isset($_SESSION['tel'])) {

    $tel = $_SESSION['tel'];

}

//prefectures の入力データの保持
if (isset($_SESSION['prefectures'])) {

    $prefectures = $_SESSION['prefectures'];

}

// hobby の入力データの保持
if (isset($_SESSION['hobby'])) {

    $hobby = $_SESSION['hobby'];

}


// gender の入力データの保持
if (isset($_SESSION['gender'])) {

    $gender = $_SESSION['gender'];


}

// contact の入力データの保持
if (isset($_SESSION['contact'])) {

    $contact = $_SESSION['contact'];


}





//
// Twig
//



// Twig_Loader_Filesystem と Twig instance の生成を読み込む
require_once('../config/twig.php');



// render
echo $twig->render('registration_form.html', array(

  'name' => $name,
  'email' => $email,
  'password' => $password,
  'tel' => $tel,
  'prefectures' => $prefectures,
  'hobby' => $hobby,
  'gender' => $gender,
  'contact' => $contact,
  'token' => $token,
  'name_errors' => $name_errors,
  'email_errors' => $email_errors,
  'password_errors' => $password_errors,
  'tel_errors' => $tel_errors,
  'prefectures_errors' => $prefectures_errors

  )
);
