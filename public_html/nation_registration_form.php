<?php

require_once('../config/config.php');

// 変数の初期化
$nation = '';
$reason = '';
$check_result = '';


if (isset($_SESSION['nation'])) {

  $nation = $_SESSION['nation'];

}

if (isset($_SESSION['reason'])) {

  $reason = $_SESSION['reason'];

}

// バリデーションエラーのセッションの受け取り
if (isset($_SESSION['check_result'])) {

  $check_result = $_SESSION['check_result'];

}



//
// Twig
//

// Composerで作成されたautoload.phpを読み込む
require_once('../vendor/autoload.php');
// Twig_Loader_Filesystem と Twig instance の生成を読み込む
require_once('../config/twig.php');

// render
echo $twig->render('nation_registration_form.html', array(

  'check_result' => $check_result,

  'nation' => $nation,
  'reason' => $reason


  )
);
